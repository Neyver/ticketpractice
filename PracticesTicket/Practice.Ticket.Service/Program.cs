﻿using Practice.Ticket.BusinessLogic;

namespace Practice.Ticket.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Attention attention = new Attention();
            attention.AttentionalProcess();          
        }
    }
}
