﻿namespace Practice.Ticket.Model
{
    public class Ticket : ITicket
    {
        public int Number { get; set; }
        public bool Attended { get; set; }
    }
}