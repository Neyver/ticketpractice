﻿namespace Practice.Ticket.Model
{
    public interface ITicket
    {
        int Number { get; set; }

        bool Attended { get; set; }
    }
}