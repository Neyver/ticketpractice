﻿namespace Practice.Ticket.Model
{
    public interface ITicketGenerator
    {
        ITicket Generate();
    }
}