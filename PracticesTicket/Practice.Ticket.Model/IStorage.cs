﻿namespace Practice.Ticket.Model
{
    public interface IStorage
    {
        ITicket Register(ITicket ticket);
    
        ITicket GetNextTicket();

        ITicket RegisterAttended(ITicket ticket);
    }
}