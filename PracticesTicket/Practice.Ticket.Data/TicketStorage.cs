﻿namespace Practice.Ticket.Data
{
    using System.Collections.Concurrent;
    using Practice.Ticket.Model;

    public class TicketStorage : IStorage
    {
        public static ConcurrentQueue<ITicket> TicketRegistered;
        public static ConcurrentQueue<ITicket> TicketsAttended = new ConcurrentQueue<ITicket>();

        public TicketStorage()
        {
            TicketRegistered = new ConcurrentQueue<ITicket>();            
        }

        public ITicket Register(ITicket ticket)
        {
            ticket.Number = TicketRegistered.Count + 1;
            ticket.Attended = false;
            TicketRegistered.Enqueue(ticket);
            return ticket;
        }

        public ITicket GetNextTicket()
        {
            ITicket ticket;
            if (TicketRegistered.TryDequeue(out ticket))
            {
                return ticket;
            }
            else
            {
                return null;
            }
        }

        public ITicket RegisterAttended(ITicket ticket)
        {                        
            TicketsAttended.Enqueue(ticket);
            return ticket;
        }
    }
}
