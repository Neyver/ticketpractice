﻿namespace Practice.Ticket.BusinessLogic
{
    using Practice.Ticket.Data;
    using Practice.Ticket.Model;

    public class TicketGenerator : ITicketGenerator
    {
        public IStorage TicketConcurrentQueue;

        public TicketGenerator ()
        {
            this.TicketConcurrentQueue = new TicketStorage();
        }

        public ITicket Generate()
        {
            return this.TicketConcurrentQueue.Register(new Ticket());
        }
    }
}