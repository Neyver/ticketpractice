﻿using System.Threading;

namespace Practice.Ticket.BusinessLogic
{
    public class Attention
    {
        public void AttentionalProcess()
        {
            CustomerSupport customerSupport = new CustomerSupport(25);            
            var window1 = new Thread(new ThreadStart(customerSupport.StartAttention)) { Name = "Window One" };
            var window2 = new Thread(new ThreadStart(customerSupport.StartAttention)) { Name = "Window Two" };
            var window3 = new Thread(new ThreadStart(customerSupport.StartAttention)) { Name = "Window Three" };
            var window4 = new Thread(new ThreadStart(customerSupport.StartAttention)) { Name = "Window Four" };
            System.Console.WriteLine($"The process has start");
            window1.Start();
            window2.Start();
            window3.Start();
            window4.Start();

            window1.Join();
            window2.Join();
            window3.Join();
            window4.Join();

            System.Console.WriteLine($"The process has finished.");
        }
    }
}
