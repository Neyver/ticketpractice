﻿using Practice.Ticket.Data;
using Practice.Ticket.Model;
using System.Threading;

namespace Practice.Ticket.BusinessLogic
{
    public class CustomerSupport
    {
        private TicketStorage ticketStorege;
        private int workingTime;
        public CustomerSupport(int workingTime)
        {
            this.ticketStorege = new TicketStorage();
            this.workingTime = workingTime;
        }

        public void StartAttention()
        {
            int cycle = 0;

            while (cycle <= (workingTime * 60))
            {
                ITicket ticket = this.ticketStorege.GetNextTicket();
                if (ticket != null)
                {
                    ticket.Attended = true;
                    Thread.Sleep(300);
                    this.ticketStorege.RegisterAttended(ticket);
                    System.Console.WriteLine($"{Thread.CurrentThread.Name} [{Thread.CurrentThread.ManagedThreadId}] Ticket Attended {ticket.Number}");
                }
                else
                {
                    System.Console.WriteLine($"{Thread.CurrentThread.Name} There is no client queued.");
                }

                cycle = cycle++;
            }
        }
    }
}
